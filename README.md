Checking out this repository and pulling the mesh files will require the installation of git large file storage:

```bash
sudo apt-get install git-lfs
```

Files:
* raven.xacro -- gazebo-independent description
* raven.teleportation.xacro -- use this for the teleportation control chain
* raven.sensors.xacro -- all sensor plugins; must be included in another file.
* raven.gazebo.xacro -- other misc gazebo stuff; pulled out b/c must be included in another file AFTER the teleportation plugin

